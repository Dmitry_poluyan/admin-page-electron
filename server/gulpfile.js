'use strict';
var gulp   = require( 'gulp' );
var server = require( 'gulp-develop-server' );

// run server
gulp.task( 'server:start', function() {
    server.listen( { path: './app.js' } );
});