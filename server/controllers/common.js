'use strict';
var GroupModel = require('../models/group');
var UserModel = require('../models/user');

var modelsMap = {
    'group': GroupModel,
    'user': UserModel
};
module.exports.checkUnique = function (req, res, next) {
    var property = req.params.property;
    var value = req.params.value.trim();
    var model = req.params.model;
    var ItemModel;

    ItemModel = modelsMap[model];

    if(ItemModel) {
        var query = {};
        query[property] = value;
        return ItemModel.findOne(query).then(function (user) {
            if(user){
                return res.json({uniqueStatus: false});
            }else{
                return res.json({uniqueStatus: true});
            }
        }).catch(next);
    }else{
        var err = new Error('Error validation unique');
        err.status = 404;
        return next(err);
    }
};