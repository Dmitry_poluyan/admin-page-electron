(function () {
    'use strict';
    angular
        .module('adminPage.user')
        .controller('AddNewUserCtrl', AddNewUserCtrl);

    AddNewUserCtrl.$inject = ['$log','$state','growl','userService'];

    function AddNewUserCtrl ($log, $state, growl, userService) {
        var vm = this;

        vm.addNewUser = addNewUser;

        function addNewUser(userDetails, isvalid){
            if (isvalid) {
                userService.addNewUser(userDetails).then(function(){
                    growl.success('User add success', {title: 'Success!'});
                    $state.go('usersPage');
                }).catch(function(rej){
                    $log.debug('AddNewUserCtrl: addNewUser: addNewUser: ERROR');
                    $log.debug(rej);
                });
            }
        }

    }
})();