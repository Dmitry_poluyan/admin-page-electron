(function () {
    'use strict';
    angular
        .module('adminPage.user')
        .controller('UserEditCtrl',UserEditCtrl);

    UserEditCtrl.$inject = ['$log','$state','growl','userService','user'];

    function UserEditCtrl ($log, $state, growl, userService, user) {
        var vm = this;

        vm.user = user;

        vm.userEdit = userEdit;

        function userEdit(userDetails, isvalid){
            if (isvalid) {
                userService.editUser(userDetails).then(function(){
                    growl.success('User edit success.', {title: 'Success!'});
                    $state.go('userDetails.userProfile',{userId: userDetails._id});
                }).catch(function(rej){
                    $log.debug('UserEditCtrl: userEdit: editUser: ERROR');
                    $log.debug(rej);
                });
            }
        }
    }

})();