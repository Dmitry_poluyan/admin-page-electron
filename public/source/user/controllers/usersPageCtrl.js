(function () {
    'use strict';
    angular
        .module('adminPage')
        .controller('UsersPageCtrl', UsersPageCtrl);

    UsersPageCtrl.$inject = ['$log','userService','paginationService'];

    function UsersPageCtrl ($log, userService, paginationService) {
        var vm = this;

        vm.showUsers = true;

        vm.showNumPage = showNumPage;
        vm.searchUsers = searchUsers;

        paginationService.setPaginationOptions(0,0,5);
        vm.currentPages = paginationService.getPaginationOptions().currentPage;

        vm.showNumPage();

        function showNumPage(numPage){
            if(numPage !== vm.currentPages){
                var options = paginationService.getPaginationOptions(numPage);
                vm.currentPages = options.currentPage;
                userService.getUsersForPagination(options).then(function(data){
                    if(data.users.length !== 0){
                        vm.users = data.users;
                        vm.showUsers = true;
                    }else{
                        vm.showUsers = false;
                    }
                    paginationService.setCountLinks(data.count);
                    vm.paginationList = paginationService.getPaginationList();
                }).catch(function(rej){
                    $log.debug('UsersPageCtrl: showNumPage: getUsersForPagination: ERROR');
                    $log.debug(rej);
                });
            }
        }

        function searchUsers(searchParams){
            if(searchParams){
                userService.searchUsers(searchParams).then(function (users) {
                    vm.searchUsersResult = users;
                }).catch(function (rej) {
                    $log.debug('UsersPageCtrl: searchUsers: searchUsers: ERROR');
                    $log.debug(rej);
                });
            }else{
                vm.searchUsersResult = '';
            }
        }
    }
})();