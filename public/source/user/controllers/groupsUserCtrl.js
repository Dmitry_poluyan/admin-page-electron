(function () {
    'use strict';
    angular
        .module('adminPage.user')
        .controller('GroupsUserCtrl', GroupsUserCtrl);

    GroupsUserCtrl.$inject = ['$log','$state','groupService','userService','growl','alertify','userId'];

    function GroupsUserCtrl ($log, $state, groupService, userService, growl, alertify, userId) {
        var vm = this;

        vm.showUserGroups = true;

        vm.addGroupForUser = addGroupForUser;
        vm.deleteGroupFromUser = deleteGroupFromUser;

        getGroupsUser(userId);
        getGroups();

        function getGroupsUser(userId){
            groupService.getGroupsUser(userId).then(function(groups){
                if(groups.length !== 0){
                    vm.groupsUser = groups;
                    vm.showUserGroups = true;
                }else{
                    vm.showUserGroups = false;
                }
            }).catch(function (rej){
                $log.debug('GroupsUserCtrl: getGroupsUser: getGroupsUser ERROR');
                $log.debug(rej);
            });
        }

        function getGroups () {
            groupService.getAllGroups().then(function(groups){
                vm.groups = groups;
            }).catch(function(rej){
                $log.debug('GroupsUserCtrl: getGroups: getAllGroups: ERROR');
                $log.debug(rej);
            });
        }

        function addGroupForUser(details, isvalid, form){
            if (isvalid) {
                userService.addGroupForUser(details.group._id, userId).then(function(){
                    form.$setPristine();
                    form.$setUntouched();
                    vm.groupForUser = '';
                    growl.success('Group for user add success', {title: 'Success!'});
                    $state.go('userDetails.groupsUser.groupsList');
                    getGroupsUser(userId);
                }).catch(function(rej){
                    $log.debug('GroupsUserCtrl: addGroupForUser: addGroupForUser: ERROR');
                    $log.debug(rej);
                });
            }
        }

        function deleteGroupFromUser(groupId){
            alertify.confirm('Are you sure you want to delete the group?', function () {
                userService.deleteGroupFromUser(userId, groupId).then(function () {
                    getGroupsUser(userId);
                    growl.success('User group deleted', {title: 'Success!'});
                }).catch(function(rej){
                    $log.debug('GroupsUserCtrl: deleteGroupFromUser: deleteGroupFromUser ERROR');
                    $log.debug(rej);
                });
            });
        }

    }
})();