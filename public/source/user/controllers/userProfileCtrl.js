(function () {
    'use strict';
    angular
        .module('adminPage.user')
        .controller('UserProfileCtrl', UserProfileCtrl);

    UserProfileCtrl.$inject = ['user'];

    function UserProfileCtrl (user) {
        var vm = this;

        vm.user = user;
    }
})();