(function () {
    'use strict';
    angular
        .module('adminPage.user')
        .controller('UserDetailsCtrl', UserDetailsCtrl);

    UserDetailsCtrl.$inject = ['$log','growl','$state','userService','alertify','user'];

    function UserDetailsCtrl ($log, growl, $state, userService, alertify, user) {
        var vm = this;

        vm.user = user;

        vm.deleteUser = deleteUser;

        function deleteUser(userId){
            alertify.confirm('Are you sure you want to delete the user?', function () {
                userService.deleteUser(userId).then(function () {
                    $state.go('usersPage');
                    growl.success('User deleted', {title: 'Success!'});
                }).catch(function(rej){
                    $log.debug('UserDetailsCtrl: deleteUser: deleteUser ERROR');
                    $log.debug(rej);
                });
            });
        }
    }

})();