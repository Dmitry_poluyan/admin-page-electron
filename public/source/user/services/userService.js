(function () {
    'use strict';
    angular
        .module('adminPage.user')
        .factory('userService',userService);

    userService.$inject = ['$http','$log'];

    function userService ($http, $log) {
        var serviceBaseURI = 'http://localhost:3000/api/users';

        return {
            getUsersForPagination: getUsersForPagination,
            getUserById: getUserById,
            editUser: editUser,
            addNewUser: addNewUser,
            addGroupForUser: addGroupForUser,
            deleteUser: deleteUser,
            deleteGroupFromUser: deleteGroupFromUser,
            getUsersGroup: getUsersGroup,
            searchUsers: searchUsers
        };

        function getUsersForPagination(options){
            return $http({
                method: 'GET',
                url: serviceBaseURI + '/usersForPagination',
                params: options
            }).then(function(res){
                $log.debug('userService: getUsersForPagination: res:');
                $log.debug(res);
                return res.data;
            });
        }

        function getUserById(userId){
            return $http({
                method: 'GET',
                url: serviceBaseURI + '/' + userId
            }).then(function(res){
                $log.debug('userService: getUserById: res:');
                $log.debug(res);
                return res.data.user;
            });
        }

        function editUser(userDetails){
            return $http({
                method: 'PUT',
                url: serviceBaseURI + '/' + userDetails._id,
                data: userDetails
            });
        }

        function addNewUser(userDetails){
            return $http({
                method: 'POST',
                url: serviceBaseURI + '/',
                data: userDetails
            });
        }

        function addGroupForUser(groupId, userId){
            return $http({
                method: 'PUT',
                url: serviceBaseURI + '/' + userId + '/addGroup/' + groupId
            });
        }

        function deleteUser(userId){
            return $http({
                method: 'DELETE',
                url: serviceBaseURI + '/'+userId
            });
        }

        function deleteGroupFromUser(userId, groupId){
            return $http({
                method: 'PUT',
                url: serviceBaseURI + '/'+userId+'/deleteGroup/'+groupId
            }).then(function(res){
                $log.debug('userService: deleteGroupFromUser: res:');
                $log.debug(res);
                return res;
            });
        }

        function getUsersGroup(groupId){
            return $http({
                method: 'GET',
                url: serviceBaseURI + '/usersGroup/' + groupId
            }).then(function(res){
                $log.debug('userService: getUsersGroup: res:');
                $log.debug(res);
                return res.data.users;
            });
        }

        function searchUsers(searchParams){
            return $http({
                method: 'GET',
                url: serviceBaseURI + '/search/' + searchParams
            }).then(function(res){
                $log.debug('userService: searchUsers: res:');
                $log.debug(res);
                return res.data.usersSearchResult;
            });
        }

    }

})();