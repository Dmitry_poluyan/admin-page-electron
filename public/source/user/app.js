(function () {
    'use strict';
    angular.module('adminPage.user', [
        'adminPage.group',
        'adminPage.common',
        'ui.router','ngMessages',
        'angular-growl',
        'ncy-angular-breadcrumb',
        'ngAlertify']);
})();