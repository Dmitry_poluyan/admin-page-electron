(function () {
    'use strict';
    angular
        .module('adminPage.user')
        .directive('admUsersList', admUsersList);

    function admUsersList(){
        return {
            restrict: 'E',
            scope: {},
            bindToController: {
                users: '=',
                isShow: '='
            },
            templateUrl: 'usersList-table.tmpl.html',
            controller: UsersListCtrl,
            controllerAs: 'vm'
        };
    }

    function UsersListCtrl(){

    }

})();