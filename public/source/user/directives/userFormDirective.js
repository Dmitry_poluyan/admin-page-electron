(function () {
    'use strict';
    angular
        .module('adminPage')
        .directive('admUserForm', admUserForm);

    function admUserForm(){
        return {
            restrict: 'E',
            scope: {},
            bindToController: {
                user: '=?',
                submitFunc: '='
            },
            templateUrl: 'userForm.tmpl.html',
            controller: UserFormCtrl,
            controllerAs: 'vm'
        };
    }

    UserFormCtrl.$inject = ['$window'];

    function UserFormCtrl($window){
        var vm = this;

        vm.back = back;

        function back(){
            $window.history.back();
        }
    }

})();
