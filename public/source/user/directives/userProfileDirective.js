(function () {
    'use strict';
    angular
        .module('adminPage.user')
        .directive('admUserProfile', admUserProfile);

    function admUserProfile(){
        return {
            restrict: 'E',
            scope: {},
            bindToController: {
                user: '='
            },
            templateUrl: 'userProfile.tmpl.html',
            controller: UserProfileCtrl,
            controllerAs: 'vm'
        };
    }

    function UserProfileCtrl(){

    }

})();