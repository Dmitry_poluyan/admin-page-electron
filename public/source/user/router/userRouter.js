(function () {
    'use strict';
    angular
        .module('adminPage.user')
        .config(configLink);

    configLink.$inject = ['$stateProvider'];

    function configLink ($stateProvider) {

        $stateProvider
            .state('usersPage', {
                url: '/users',
                templateUrl: 'usersPage.html',
                controller:'UsersPageCtrl',
                controllerAs: 'usersPage',
                ncyBreadcrumb: {
                    label: 'Users'
                }
            })
            .state('addNewUser', {
                url: '/user',
                templateUrl: 'addNewUser.html',
                controller:'AddNewUserCtrl',
                controllerAs: 'addNewUser',
                ncyBreadcrumb: {
                    label: 'Add new user',
                    parent: 'usersPage'
                }
            })
            .state('userDetails', {
                url: '/users/:userId',
                abstract: true,
                templateUrl: 'userDetailsPage.html',
                controller:'UserDetailsCtrl',
                controllerAs: 'userDetails',
                resolve:{
                    userId: resolveReturnUserId,
                    user: resolveGetUserById
                },
                ncyBreadcrumb: {
                    parent: 'usersPage'
                }
            })
            .state('userDetails.userProfile', {
                url: '/profile',
                views:{
                    content:{
                        templateUrl: 'userProfile.html',
                        controller:'UserProfileCtrl',
                        controllerAs: 'userProfile'
                    },
                    navbar:{
                        templateUrl: 'details-user-navbar.html'
                    }
                },
                ncyBreadcrumb: {
                    label: '{{userDetails.user.userName}}'
                }
            })
            .state('userDetails.userEdit', {
                url: '/edit',
                views:{
                    content:{
                        templateUrl: 'userEdit.html',
                        controller:'UserEditCtrl',
                        controllerAs: 'userEdit'
                    },
                    navbar:{
                        templateUrl: 'details-user-navbar.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Edit',
                    parent: 'userDetails.userProfile'
                }
            })
            .state('userDetails.groupsUser', {
                url: '/groups',
                abstract: true,
                views:{
                    content:{
                        templateUrl: 'groupsUser.html',
                        controller:'GroupsUserCtrl',
                        controllerAs: 'groupsUser'
                    },
                    navbar:{
                        templateUrl: 'details-user-navbar.html'
                    }
                },
                ncyBreadcrumb: {
                    parent: 'userDetails.userProfile'
                }
            })
            .state('userDetails.groupsUser.groupsList', {
                url: '/list',
                views:{
                    content:{
                        templateUrl: 'groupsUser-list.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Groups list'
                }
            })
            .state('userDetails.groupsUser.addGroup', {
                url: '/add',
                views:{
                    content:{
                        templateUrl: 'groupsUser-addGForU.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Groups add'
                }
            });

        resolveReturnUserId.$inject = ['$stateParams'];
        function resolveReturnUserId($stateParams){
            return $stateParams.userId;
        }

        resolveGetUserById.$inject = ['userService','userId','$log'];
        function resolveGetUserById(userService, userId, $log){
            return userService.getUserById(userId).then(function(user){
                return user;
            }).catch(function(rej){
                $log.debug('userRouter: resolveGetUserById: getUserById: ERROR');
                $log.debug(rej);
                return rej;
            });
        }
    }
})();