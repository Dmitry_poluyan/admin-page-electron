(function () {
    'use strict';
    angular.module('adminPage', [
        'adminPage.user',
        'adminPage.group',
        'adminPage.common',
        'ngAnimate'
    ])
        .config(growlProviders);

    growlProviders.$inject = ['growlProvider', '$httpProvider'];

    function growlProviders(growlProvider, $httpProvider) {
        growlProvider.globalTimeToLive(3000);
        growlProvider.messagesKey('growlMessages');
        growlProvider.messageTextKey('text');
        growlProvider.messageTitleKey('tittle');
        growlProvider.messageSeverityKey('severity');
        $httpProvider.interceptors.push(growlProvider.serverMessagesInterceptor);
    }

})();