(function () {
    'use strict';
    angular.module('adminPage.common', [
        'ui.router',
        'ngMessages',
        'angular-growl',
        'ncy-angular-breadcrumb',
        'ngAlertify']);
})();