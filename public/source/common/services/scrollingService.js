(function () {
    'use strict';
    angular
        .module('adminPage.common')
        .factory('scrollingService', scrollingService);

    function scrollingService(){
        var options = {};
        var allCountItems;

        return{
            setScrollingOptions: setScrollingOptions,
            setCountLinks: setCountLinks,
            getScrollingOptions: getScrollingOptions,
            getTotalPagesNum: getTotalPagesNum,
            getPageLinks: getPageLinks
        };

        function setScrollingOptions(currentPage, skipPage, itemsOnPage){
            options = {
                currentPage: currentPage,
                skipPage: skipPage,
                itemsOnPage: itemsOnPage
            };
        }

        function setCountLinks(countItems){
            allCountItems = {
                countLinks:countItems
            };
        }

        function getScrollingOptions(){
            return options;
        }

        function getTotalPagesNum(){
            return Math.ceil(allCountItems.countLinks/options.itemsOnPage);
        }

        function getPageLinks(){
            options = {
                currentPage: options.currentPage,
                skipPage: options.itemsOnPage*options.currentPage,
                itemsOnPage: options.itemsOnPage
            };
            return options;
        }
    }

})();