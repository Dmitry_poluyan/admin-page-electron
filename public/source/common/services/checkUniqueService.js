(function () {
    'use strict';
    angular.module('adminPage.common')
        .factory('checkUniqueService', checkUniqueService);

    checkUniqueService.$inject = ['$http','$log'];

    function checkUniqueService($http, $log){
        var serviceBaseURI = 'http://localhost:3000/api/common/';

        return {
            checkUniqueValue: checkUniqueValue
        };

        function checkUniqueValue(model, property, value){
            return $http.get(serviceBaseURI + 'checkUnique/' + model + '/' + property + '/' + escape(value))
                .then(function (res) {
                    $log.debug('checkUniqueService: checkUniqueValue');
                    $log.debug(res);
                    return res.data.uniqueStatus;
                });
        }


    }
})();