(function () {
    'use strict';
    angular
        .module('adminPage.common')
        .directive('admScrolling', admScrolling);

    function admScrolling(){
        return {
            restrict: 'A',
            link: link
        };

        function link(scope, element, attr){
            var raw = element[0];
            element.bind('scroll', function() {
                if (raw.scrollTop + raw.offsetHeight + 20>= raw.scrollHeight) {
                    scope.$apply(attr.admScrolling);
                }
            });
        }
    }

})();
