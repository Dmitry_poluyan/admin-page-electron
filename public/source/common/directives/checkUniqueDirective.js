(function () {
    'use strict';
    angular.module('adminPage.common')
        .directive('checkUnique', checkUnique);

    checkUnique.$inject = ['$log','checkUniqueService'];

    function checkUnique($log, checkUniqueService){
        return {
            restrict: 'A',
            require: 'ngModel',
            link: link
        };

        function link(scope, element, attrs, ngModelCtrl){
            element.bind('blur', function () {
                if (!ngModelCtrl || !element.val()) {return;}

                var options = scope.$eval(attrs.checkUnique);
                var currentValue = element.val();

                checkUniqueService.checkUniqueValue(options.model, options.property, currentValue)
                    .then(function (unique) {
                        if (currentValue === element.val()) {
                            ngModelCtrl.$setValidity('unique', unique);
                        }
                    }).catch(function (rej) {
                        $log.debug('checkUnique: checkUniqueService: checkUniqueValue ERROR');
                        $log.debug(rej);
                        ngModelCtrl.$setValidity('unique', true);
                    });
            });
        }
    }

})();
