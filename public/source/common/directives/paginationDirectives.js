(function () {
    'use strict';
    angular
        .module('adminPage.common')
        .directive('admPagination', admPagination);

    function admPagination(){
        return {
            restrict: 'E',
            scope: {},
            bindToController: {
                currentPages: '=',
                showNumPage: '=',
                paginationList: '='
            },
            templateUrl: 'pagination.tmpl.html',
            controller: PaginationCtrl,
            controllerAs: 'vm'
        };
    }

    function PaginationCtrl(){

    }

})();
