(function () {
    'use strict';
    angular
        .module('adminPage.common')
        .directive('admSearch', admSearch);

    function admSearch(){
        return {
            restrict: 'E',
            transclude: true,
            scope: {},
            bindToController: {
                searchItemsResult: '=',
                searchItemsFunc: '='
            },
            templateUrl: 'search.tmpl.html',
            controller: SearchCtrl,
            controllerAs: 'vm'
        };
    }

    function SearchCtrl(){
        var vm = this;

        vm.clearSearch = clearSearch;

        function clearSearch(){
            vm.searchItemsResult = '';
            vm.searchParams = '';
        }
    }

})();
