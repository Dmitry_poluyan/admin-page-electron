(function () {
    'use strict';
    angular
        .module('adminPage.common')
        .config(configLink);

    configLink.$inject = ['$urlRouterProvider'];

    function configLink ($urlRouterProvider) {
        $urlRouterProvider.otherwise('/users');
    }

})();