(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .config(configLink);

    configLink.$inject = ['$stateProvider'];

    function configLink ($stateProvider) {

        $stateProvider
            .state('groupsPage', {
                url: '/groups',
                templateUrl: 'groupsPage.html',
                controller:'GroupsPageCtrl',
                controllerAs: 'groupsPage',
                ncyBreadcrumb: {
                    label: 'Groups'
                }
            })
            .state('addNewGroup', {
                url: '/group',
                templateUrl: 'addNewGroup.html',
                controller:'AddNewGroupCtrl',
                controllerAs: 'addNewGroup',
                ncyBreadcrumb: {
                    label: 'Add new group',
                    parent: 'groupsPage'
                }
            })
            .state('groupDetails', {
                url: '/groups/:groupId',
                abstract: true,
                templateUrl: 'groupDetailsPage.html',
                controller:'GroupDetailsCtrl',
                controllerAs: 'groupDetails',
                resolve:{
                    groupId: resolveReturnGroupId,
                    group: resolveGetGroupById
                },
                ncyBreadcrumb: {
                    parent: 'groupsPage'
                }
            })
            .state('groupDetails.groupProfile', {
                url: '/profile',
                views:{
                    content:{
                        templateUrl: 'groupProfile.html',
                        controller:'GroupProfileCtrl',
                        controllerAs: 'groupProfile'
                    },
                    navbar:{
                        templateUrl: 'details-group-navbar.html'
                    }
                },
                ncyBreadcrumb: {
                    label: '{{groupDetails.group.groupName}}'
                }
            })
            .state('groupDetails.groupEdit', {
                url: '/edit',
                views:{
                    content:{
                        templateUrl: 'groupEdit.html',
                        controller:'GroupEditCtrl',
                        controllerAs: 'groupEdit'
                    },
                    navbar:{
                        templateUrl: 'details-group-navbar.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Edit',
                    parent: 'groupDetails.groupProfile'
                }
            })
            .state('groupDetails.usersGroup', {
                url: '/users',
                views:{
                    content:{
                        templateUrl: 'usersGroup.html',
                        controller:'UsersGroupCtrl',
                        controllerAs: 'usersGroup'
                    },
                    navbar:{
                        templateUrl: 'details-group-navbar.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Users group',
                    parent: 'groupDetails.groupProfile'
                }
            });

        resolveReturnGroupId.$inject = ['$stateParams'];
        function resolveReturnGroupId($stateParams){
            return $stateParams.groupId;
        }

        resolveGetGroupById.$inject = ['groupService','groupId','$log'];
        function resolveGetGroupById(groupService, groupId, $log){
            return groupService.getGroupById(groupId).then(function(group){
                return group;
            }).catch(function(rej){
                $log.debug('groupRouter: resolveGetGroupById: getGroupById: ERROR');
                $log.debug(rej);
                return rej;
            });
        }
    }
})();