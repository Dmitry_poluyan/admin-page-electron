(function () {
    'use strict';
    angular.module('adminPage.group', [
        'adminPage.user',
        'adminPage.common',
        'ui.router',
        'ngMessages',
        'angular-growl',
        'ncy-angular-breadcrumb',
        'ngAlertify']);
})();