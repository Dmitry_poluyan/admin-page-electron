(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .controller('GroupsPageCtrl', GroupsPageCtrl);

    GroupsPageCtrl.$inject = ['$log','groupService','scrollingService'];

    function GroupsPageCtrl ($log, groupService, scrollingService) {
        var vm = this;

        vm.showGroups = true;

        vm.showNumPage = showNumPage;
        vm.searchGroups = searchGroups;

        scrollingService.setScrollingOptions(0,0,5);
        var options = scrollingService.getScrollingOptions();

        loadGroups();

        function loadGroups(){
            groupService.getGroupsForPagination(options).then(function(data){
                if(data.groups.length !== 0){
                    vm.groups = [];
                    angular.forEach(data.groups, function (val) {
                        vm.groups.push(val);
                    });
                    vm.showGroups = true;
                }else{
                    vm.showGroups = false;
                }
                scrollingService.setCountLinks(data.count);
            }).catch(function(rej){
                $log.debug('GroupsPageCtrl: loadGroups: getAllGroupsForScrolling: ERROR');
                $log.debug(rej);
            });
        }

        function showNumPage(){
            if(options.currentPage < scrollingService.getTotalPagesNum()-1){

                options.currentPage = ++options.currentPage;
                options = scrollingService.getPageLinks();

                groupService.getGroupsForPagination(options).then(function(data){
                    angular.forEach(data.groups, function (val) {
                        vm.groups.push(val);
                    });
                }).catch(function(rej){
                    $log.debug('GroupsPageCtrl: showNumPage: getGroupsForPagination:  ERROR');
                    $log.debug(rej);
                });
            }
        }

        function searchGroups(searchParams){
            if(searchParams){
                groupService.searchGroups(searchParams).then(function (groups) {
                    vm.searchGroupsResult = groups;
                }).catch(function (rej) {
                    $log.debug('GroupsPageCtrl: searchGroups: searchGroups: ERROR');
                    $log.debug(rej);
                });
            }else{
                vm.searchGroupsResult = '';
            }
        }

    }
})();