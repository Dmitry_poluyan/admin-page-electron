(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .controller('GroupDetailsCtrl', GroupDetailsCtrl);

    GroupDetailsCtrl.$inject = ['$log','$state', 'growl','groupService','alertify','group'];

    function GroupDetailsCtrl ($log, $state, growl, groupService, alertify, group) {
        var vm = this;

        vm.group = group;

        vm.deleteGroup = deleteGroup;

        function deleteGroup(groupId){
            alertify.confirm('Are you sure you want to delete the group?', function () {
                groupService.deleteGroup(groupId).then(function () {
                    $state.go('groupsPage');
                    growl.success('Group deleted', {title: 'Success!'});
                }).catch(function(rej){
                    $log.debug('GroupDetailsCtrl: deleteGroup: deleteGroup ERROR');
                    $log.debug(rej);
                });
            });
        }
    }

})();