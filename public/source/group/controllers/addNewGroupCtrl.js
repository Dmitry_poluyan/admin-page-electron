(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .controller('AddNewGroupCtrl', AddNewGroupCtrl);

    AddNewGroupCtrl.$inject = ['$log','$state','growl','groupService'];

    function AddNewGroupCtrl ($log, $state, growl, groupService) {
        var vm = this;

        vm.addNewGroup = addNewGroup;

        function addNewGroup(groupDetails, isvalid){
            if (isvalid) {
                groupService.addNewGroup(groupDetails).then(function(){
                    growl.success('Group add success', {title: 'Success!'});
                    $state.go('groupsPage');
                }).catch(function(rej){
                    $log.debug('GroupsPageCtrl: addNewGroup: addNewGroup: ERROR');
                    $log.debug(rej);
                });
            }
        }
    }

})();