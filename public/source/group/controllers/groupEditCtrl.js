(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .controller('GroupEditCtrl', GroupEditCtrl);

    GroupEditCtrl.$inject = ['$log','$state', 'growl','groupService','group'];

    function GroupEditCtrl ($log, $state, growl, groupService, group) {
        var vm = this;

        vm.group = group;

        vm.groupEdit = groupEdit;

        function groupEdit(groupDetails, isvalid){
            if (isvalid) {
                groupService.editGroup(groupDetails).then(function(){
                    growl.success('Group edit success.', {title: 'Success!'});
                    $state.go('groupDetails.groupProfile',{groupId: groupDetails._id});
                }).catch(function(rej){
                    $log.debug('GroupEditCtrl: groupEdit: editGroup: ERROR');
                    $log.debug(rej);
                });
            }
        }

    }

})();