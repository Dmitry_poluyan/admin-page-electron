(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .controller('UsersGroupCtrl', UsersGroupCtrl);

    UsersGroupCtrl.$inject = ['$log','userService','groupId'];

    function UsersGroupCtrl ($log, userService, groupId) {
        var vm = this;

        vm.showUsersGroup = true;

        getUsersGroup(groupId);

        function getUsersGroup(groupId){
            userService.getUsersGroup(groupId).then(function(users){
                if(users.length !== 0){
                    vm.usersGroup = users;
                    vm.showUsersGroup = true;
                }else{
                    vm.showUsersGroup = false;
                }
            }).catch(function(rej){
                $log.debug('usersGroupCtrl: getUsersGroup: getUsersGroup: ERROR');
                $log.debug(rej);
            });
        }
    }

})();