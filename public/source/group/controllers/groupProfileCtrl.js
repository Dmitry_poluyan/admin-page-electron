(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .controller('GroupProfileCtrl', GroupProfileCtrl);

    GroupProfileCtrl.$inject = ['group'];

    function GroupProfileCtrl (group) {
        var vm = this;

        vm.group = group;
    }

})();