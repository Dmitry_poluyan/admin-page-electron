(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .factory('groupService', groupService);

    groupService.$inject = ['$http','$log'];

    function groupService ($http, $log) {
        var serviceBaseURI = 'http://localhost:3000/api/groups';

        return {
            getGroupsForPagination: getGroupsForPagination,
            getAllGroups: getAllGroups,
            getGroupById: getGroupById,
            editGroup: editGroup,
            addNewGroup: addNewGroup,
            deleteGroup: deleteGroup,
            getGroupsUser: getGroupsUser,
            searchGroups: searchGroups
        };

        function getGroupsForPagination(options){
            return $http({
                method: 'GET',
                url: serviceBaseURI + '/groupsForPagination',
                params: options
            }).then(function(res){
                $log.debug('groupService: getGroupsForPagination: res:');
                $log.debug(res);
                return res.data;
            });
        }

        function getAllGroups(){
            return $http({
                method: 'GET',
                url: serviceBaseURI + '/'
            }).then(function(res){
                $log.debug('groupService: getAllGroups: res:');
                $log.debug(res);
                return res.data.groups;
            });
        }

        function getGroupById(groupId){
            return $http({
                method: 'GET',
                url: serviceBaseURI + '/' + groupId
            }).then(function(res){
                $log.debug('groupService: getGroupById: res:');
                $log.debug(res);
                return res.data.group;
            });
        }

        function editGroup(groupDetails){
            return $http({
                method: 'PUT',
                url: serviceBaseURI + '/'+ groupDetails._id,
                data: groupDetails
            });
        }

        function addNewGroup(groupDetails){
            return $http({
                method: 'POST',
                url: serviceBaseURI + '/',
                data: groupDetails
            });
        }

        function deleteGroup(groupId){
            return $http({
                method: 'DELETE',
                url: serviceBaseURI + '/' + groupId
            });
        }

        function getGroupsUser(userId){
            return $http({
                method: 'GET',
                url: serviceBaseURI + '/groupsUser/' + userId
            }).then(function(res){
                $log.debug('groupService: getUserGroups: res:');
                $log.debug(res);
                return res.data.groups;
            });
        }

        function searchGroups(searchParams){
            return $http({
                method: 'GET',
                url: serviceBaseURI + '/search/' + searchParams
            }).then(function(res){
                $log.debug('groupService: searchGroups: res:');
                $log.debug(res);
                return res.data.groupsSearchResult;
            });
        }
    }

})();