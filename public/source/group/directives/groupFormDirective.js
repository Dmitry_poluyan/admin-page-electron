(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .directive('admGroupForm', admGroupForm);

    function admGroupForm(){
        return {
            restrict: 'E',
            scope: {},
            bindToController: {
                group: '=?',
                submitFunc: '='
            },
            templateUrl: 'groupForm.tmpl.html',
            controller: GroupFormCtrl,
            controllerAs: 'vm'
        };
    }

    GroupFormCtrl.$inject = ['$window'];

    function GroupFormCtrl($window){
        var vm = this;

        vm.back = back;

        function back(){
            $window.history.back();
        }
    }

})();
