(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .directive('admGroupsList', admGroupsList);

    function admGroupsList(){
        return {
            restrict: 'E',
            scope: {},
            bindToController: {
                groups: '=',
                isShow: '=',
                deleteFunc: '=?'
            },
            templateUrl: 'groupsList-table.tmpl.html',
            controller: GroupsListCtrl,
            controllerAs: 'vm'
        };
    }

    function GroupsListCtrl(){

    }

})();