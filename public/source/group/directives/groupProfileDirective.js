(function () {
    'use strict';
    angular
        .module('adminPage.group')
        .directive('admGroupProfile', admGroupProfile);

    function admGroupProfile(){
        return {
            restrict: 'E',
            scope: {},
            bindToController: {
                group: '='
            },
            templateUrl: 'groupProfile.tmpl.html',
            controller: groupProfileCtrl,
            controllerAs: 'vm'
        };
    }

    function groupProfileCtrl(){

    }

})();
