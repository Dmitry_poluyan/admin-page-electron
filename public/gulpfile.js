'use strict';
var gulp   = require( 'gulp' );
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var templateCache = require('gulp-angular-templatecache');
var sourcemaps = require('gulp-sourcemaps');
var cssnano = require('gulp-cssnano');
var rimraf = require('rimraf');

gulp.task('js',['clean'], function () {
    gulp.src([
        'source/user/app.js',
        'source/group/app.js',
        'source/app.js',
        'source/common/**/*.js',
        'source/user/**/*.js',
        'source/group/**/*.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('app/js/'));
});

gulp.task('view',['clean'], function () {
    gulp.src([
        'source/common/directives/*.html',
        'source/common/view/*.html',
        'source/user/directives/*.html',
        'source/user/view/*.html',
        'source/group/directives/*.html',
        'source/group/view/*.html'
    ])
        .pipe(templateCache({module: 'adminPage'}))
        .pipe(gulp.dest('app/js/'));
});

gulp.task('css',['clean'], function () {
    gulp.src([
        'source/vendor/css/**/*.css'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('all.css'))
        .pipe(cssnano())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('app/css/'));
});

gulp.task('clean', function (cb) {
    rimraf('app/', cb);
});

gulp.task('bower:js',['clean'], function () {
    gulp.src([
        'source/vendor/bower_components/angular/angular.min.js',
        'source/vendor/bower_components/angular-ui-router/release/angular-ui-router.min.js',
        'source/vendor/bower_components/angular-breadcrumb/dist/angular-breadcrumb.min.js',
        'source/vendor/bower_components/angular-messages/angular-messages.min.js',
        'source/vendor/bower_components/alertify.js/dist/js/ngAlertify.js',
        'source/vendor/bower_components/angular-animate/angular-animate.min.js',
        'source/vendor/bower_components/angular-growl-v2/build/angular-growl.min.js'
    ])
        .pipe(concat('bower.js'))
        .pipe(gulp.dest('app/js/'));
});

gulp.task('bower:css',['clean'], function () {
    gulp.src([
        'source/vendor/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'source/vendor/bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
        'source/vendor/bower_components/alertify.js/dist/css/alertify.css',
        'source/vendor/bower_components/animate.css/animate.min.css',
        'source/vendor/bower_components/angular-growl-v2/build/angular-growl.min.css'
    ])
        .pipe(concat('bower.css'))
        .pipe(gulp.dest('app/css/'));
});

gulp.task('copy:index',['clean'], function () {
    gulp.src([
        'source/*.html'
    ])
        .pipe(gulp.dest('app/'));
});

gulp.task('copy:img',['clean'], function () {
    gulp.src([
        'source/vendor/img/**/*'
    ])
        .pipe(gulp.dest('app/img/'));
});

gulp.task('watch', function() {
    gulp.watch([
        'source/user/**/*.*',
        'source/common/**/*.*',
        'source/group/**/*.*'
    ], ['build']);
});

gulp.task('build', ['clean','js', 'view', 'css', 'copy:index', 'copy:img', 'bower:css','bower:js']);
